const weatherForm = document.querySelector('form');
const searchInput = document.querySelector('input');
const messageLocation = document.querySelector('#message-location');
const messageForecast = document.querySelector('#message-2');
const searchCurrentLocationButton = document.getElementById('current-location-button');
const searchButton = document.querySelector('#search-button');

weatherForm.addEventListener('submit', (event) => {
    event.preventDefault();
    searchButton.disabled = true;
    const location = searchInput.value;
    fetchForecastForLocation(location, searchButton);
});

searchCurrentLocationButton.addEventListener('click', (event) => {
    searchCurrentLocationButton.disabled = true;
    if (!navigator.geolocation) {
        return alert('Geolocation is not supported by your browser');
    }

    searchInput.value = '';
    navigator.geolocation.getCurrentPosition((position) => {
        const coords = position.coords;
        fetchForecastForLocation(coords.longitude + ',' + coords.latitude, searchCurrentLocationButton);
    });
})

function fetchForecastForLocation(address, button) {
    const url = '/weather?address=' + address;
    messageLocation.textContent = 'Searching...';
    removeAllChildNodes(document.querySelector('.forecast-content'));
    fetch(url).then((response) => {
        response.json().then((data) => {
            parseData(data);
            button.disabled = false;
        });
    });
}

function parseData(data) {
    if (data.error) {
        messageLocation.textContent = data.error;
    }
    else {
        messageLocation.textContent = data.location;
        let array = data.forecast.theForecast.split('. ');
        for (let i = 0; i < array.length; i++) {
            let node;
            const element = array[i];
            const div = document.querySelector('.forecast-content');
            const para = document.createElement('p');
            if (i == 0) {
                const img = document.createElement('img');
                img.src = data.forecast.img;
                img.id = 'weather-description-icon';
                para.appendChild(img);
                node = document.createTextNode(array[i]);
            }
            else {
                node = document.createTextNode(element);
            }
            para.appendChild(node);
            div.appendChild(para);
        }
    }
}

function removeAllChildNodes(parent) {
    while (parent.firstChild) {
        parent.removeChild(parent.firstChild);
    }
}