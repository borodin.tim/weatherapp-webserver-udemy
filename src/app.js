const path = require('path');
const express = require('express');
const hbs = require('hbs');
//const request = require('postman-request');
const chalk = require('chalk');
const geocode = require('./utils/geocode');
const forecast = require('./utils/forecast');

const app = express();
const port = process.env.PORT || 3000;

// Define path for Express config
const publicDirectoryPath = path.join(__dirname, '../public');
const viewsPath = path.join(__dirname, '../templates/views');
const partialsPath = path.join(__dirname, '../templates/partials');

// Setup handlebars engine and views location
app.set('view engine', 'hbs');
app.set('views', viewsPath);
hbs.registerPartials(partialsPath);

// Setup static directory to serve
app.use(express.static(publicDirectoryPath));

app.get('', (req, res) => {
    res.render('index', {
        title: 'Weather App',
        name: 'Tim'
    });
});

app.get('/about', (req, res) => {
    res.render('about', {
        title: 'Aboot Me',
        name: 'Tim'
    });
});

app.get('/help', (req, res) => {
    res.render('help', {
        title: 'Help',
        name: 'Tim',
        helpMsg: 'This is a help message, wololo.'
    });
});

app.get('/weather', (req, res) => {
    console.log('req.query is: ', req.query);
    if (!req.query.address && !req.query.coords) {
        return res.send({ error: 'You must provide an address or coordinates!' });
    }
    if (req.query.address) {
        geocode(req.query.address, (error, { latitude, longitude, location } = {}) => {
            if (error) { return res.send({ error }) }
            getForecast(latitude, longitude, res, location, req);
        });
    } else if (req.query.coords) {
        let arr = req.query.coords.split(',');
        getForecast(arr[0], arr[1], res, null, req);
    }
});

app.get('/products', (req, res) => {
    if (!req.query.search) {
        return res.send({ error: 'Your must provide a search term' });
    }
    console.log(req.query.search);
    res.send({
        products: [],
    });
});

app.get('/help/*', (req, res) => {
    res.render('404page', {
        title: '404 Page not found',
        errorMessage: 'Help article not found',
        name: 'Tim'
    });
});

app.get('*', (req, res) => {
    res.render('404page', {
        title: '404 Page not found',
        errorMessage: 'Page not found',
        name: 'Tim'
    });
});

app.listen(port, () => {
    console.log('Server is up on http://localhost:' + port);
}); title: 'Page not found'

function getForecast(latitude, longitude, res, location, req) {
    forecast(latitude, longitude, (error, forecastData) => {
        if (error) {
            return res.send({ error });
        }
        res.send({
            forecast: forecastData,
            location: location,
            address: req.query.address,
        });
    });
}
