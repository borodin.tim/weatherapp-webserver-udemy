const request = require('postman-request');

// Geocoding - mapbox.com
const geocode = (address, callback) => {
    const url = 'https://api.mapbox.com/geocoding/v5/mapbox.places/' + encodeURIComponent(address) + '.json?access_token=pk.eyJ1IjoiZGVsYWluIiwiYSI6ImNrZGV2cHk2eTFwZmMzNHJ4bTNwZzl3YWsifQ.eZkF9CAJ0x_7DPL9IETH-g&limit=1';
    // console.log('Geocode URL: ', url);
    request({ url, json: true }, (error, { body } = {}) => {
        // console.log('body is: ', body);
        if (error) {
            callback('Unable to connect to Geocoding service!', undefined); // undefined is not necessary
        } else if (body.message) {
            callback(body.message);
        } else if (body.features.length === 0) {
            callback('Unable to find location! Try again with a different search term');
        } else {
            // console.log('body.features[0].center: ' + body.features[0].center);
            callback(undefined, {
                latitude: body.features[0].center[1],
                longitude: body.features[0].center[0],
                location: body.features[0].place_name,
            });
            // console.log('lat:' + body.features[0].center[1]);
            // console.log('lon:' + body.features[0].center[0]);
        }
    });
};

module.exports = geocode