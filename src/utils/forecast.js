const request = require('postman-request');

const forecast = (latitude, longitude, callback) => {
    const url = 'http://api.weatherstack.com/current?access_key=b3b4c29fcbd7a7db7c68f3e28ceaca4f&query=' + latitude + ',' + longitude + '&units=m';
    request({ url, json: true }, (error, { body }) => {
        if (error) {
            callback('Unable to connect to Weather service!');
        } else if (body.error) {
            callback(body.error.info);
        } else {
            let theForecast = body.current.weather_descriptions[0] + '. ';
            theForecast += 'It is currently ' + body.current.temperature + '℃. ';
            theForecast += 'Feels like ' + body.current.feelslike + '℃. ';
            theForecast += 'Humidity: ' + body.current.humidity + '%. ';
            theForecast += 'Pressure: ' + body.current.pressure + ' mBar. ';
            theForecast += 'Precipitation: ' + body.current.precip + '%. ';
            theForecast += 'UV index: ' + body.current.uv_index + '. ';
            theForecast += 'Wind speed: ' + body.current.wind_speed + ' km/h. ';
            theForecast += 'Wind Direction: ' + body.current.wind_dir + '. ';          
            theForecast += 'Visibility: ' + body.current.visibility + ' km. ';
            theForecast += 'Observation time: ' + body.current.observation_time + '. ';
            const img = body.current.weather_icons[0];
            const res = { theForecast, img };
            callback(undefined, res);
        }
    });
};


module.exports = forecast